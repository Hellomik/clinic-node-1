import { registerEnumType } from '@nestjs/graphql';

export enum UserScalarFieldEnum {
    id = "id",
    uniqueNumber = "uniqueNumber",
    firstName = "firstName",
    lastName = "lastName",
    patronymic = "patronymic",
    dateBirth = "dateBirth",
    hash = "hash"
}


registerEnumType(UserScalarFieldEnum, { name: 'UserScalarFieldEnum', description: undefined })
