import { Field } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { PreUserWhereInput } from '../pre-user/pre-user-where.input';
import { Type } from 'class-transformer';
import { PreUserOrderByWithAggregationInput } from '../pre-user/pre-user-order-by-with-aggregation.input';
import { PreUserScalarFieldEnum } from '../pre-user/pre-user-scalar-field.enum';
import { PreUserScalarWhereWithAggregatesInput } from '../pre-user/pre-user-scalar-where-with-aggregates.input';
import { Int } from '@nestjs/graphql';

@ArgsType()
export class GroupByPreUserArgs {

    @Field(() => PreUserWhereInput, {nullable:true})
    @Type(() => PreUserWhereInput)
    where?: PreUserWhereInput;

    @Field(() => [PreUserOrderByWithAggregationInput], {nullable:true})
    orderBy?: Array<PreUserOrderByWithAggregationInput>;

    @Field(() => [PreUserScalarFieldEnum], {nullable:false})
    by!: Array<keyof typeof PreUserScalarFieldEnum>;

    @Field(() => PreUserScalarWhereWithAggregatesInput, {nullable:true})
    having?: PreUserScalarWhereWithAggregatesInput;

    @Field(() => Int, {nullable:true})
    take?: number;

    @Field(() => Int, {nullable:true})
    skip?: number;
}
