import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { StringWithAggregatesFilter } from '../prisma/string-with-aggregates-filter.input';
import { StringNullableWithAggregatesFilter } from '../prisma/string-nullable-with-aggregates-filter.input';
import { DateTimeNullableWithAggregatesFilter } from '../prisma/date-time-nullable-with-aggregates-filter.input';

@InputType()
export class PreUserScalarWhereWithAggregatesInput {

    @Field(() => [PreUserScalarWhereWithAggregatesInput], {nullable:true})
    AND?: Array<PreUserScalarWhereWithAggregatesInput>;

    @Field(() => [PreUserScalarWhereWithAggregatesInput], {nullable:true})
    OR?: Array<PreUserScalarWhereWithAggregatesInput>;

    @Field(() => [PreUserScalarWhereWithAggregatesInput], {nullable:true})
    NOT?: Array<PreUserScalarWhereWithAggregatesInput>;

    @Field(() => StringWithAggregatesFilter, {nullable:true})
    id?: StringWithAggregatesFilter;

    @Field(() => StringWithAggregatesFilter, {nullable:true})
    uniqueNumber?: StringWithAggregatesFilter;

    @Field(() => StringWithAggregatesFilter, {nullable:true})
    photoUrl?: StringWithAggregatesFilter;

    @Field(() => StringNullableWithAggregatesFilter, {nullable:true})
    firstName?: StringNullableWithAggregatesFilter;

    @Field(() => StringNullableWithAggregatesFilter, {nullable:true})
    lastName?: StringNullableWithAggregatesFilter;

    @Field(() => StringNullableWithAggregatesFilter, {nullable:true})
    patronymic?: StringNullableWithAggregatesFilter;

    @Field(() => DateTimeNullableWithAggregatesFilter, {nullable:true})
    dateBirth?: DateTimeNullableWithAggregatesFilter;
}
