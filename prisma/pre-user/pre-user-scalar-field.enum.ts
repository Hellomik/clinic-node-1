import { registerEnumType } from '@nestjs/graphql';

export enum PreUserScalarFieldEnum {
    id = "id",
    uniqueNumber = "uniqueNumber",
    photoUrl = "photoUrl",
    firstName = "firstName",
    lastName = "lastName",
    patronymic = "patronymic",
    dateBirth = "dateBirth"
}


registerEnumType(PreUserScalarFieldEnum, { name: 'PreUserScalarFieldEnum', description: undefined })
