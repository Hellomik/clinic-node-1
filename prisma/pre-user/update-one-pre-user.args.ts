import { Field } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { PreUserUpdateInput } from './pre-user-update.input';
import { Type } from 'class-transformer';
import { PreUserWhereUniqueInput } from './pre-user-where-unique.input';

@ArgsType()
export class UpdateOnePreUserArgs {

    @Field(() => PreUserUpdateInput, {nullable:false})
    @Type(() => PreUserUpdateInput)
    data!: PreUserUpdateInput;

    @Field(() => PreUserWhereUniqueInput, {nullable:false})
    @Type(() => PreUserWhereUniqueInput)
    where!: PreUserWhereUniqueInput;
}
