import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { StringFilter } from '../prisma/string-filter.input';
import { StringNullableFilter } from '../prisma/string-nullable-filter.input';
import { DateTimeNullableFilter } from '../prisma/date-time-nullable-filter.input';

@InputType()
export class PreUserWhereInput {

    @Field(() => [PreUserWhereInput], {nullable:true})
    AND?: Array<PreUserWhereInput>;

    @Field(() => [PreUserWhereInput], {nullable:true})
    OR?: Array<PreUserWhereInput>;

    @Field(() => [PreUserWhereInput], {nullable:true})
    NOT?: Array<PreUserWhereInput>;

    @Field(() => StringFilter, {nullable:true})
    id?: StringFilter;

    @Field(() => StringFilter, {nullable:true})
    uniqueNumber?: StringFilter;

    @Field(() => StringFilter, {nullable:true})
    photoUrl?: StringFilter;

    @Field(() => StringNullableFilter, {nullable:true})
    firstName?: StringNullableFilter;

    @Field(() => StringNullableFilter, {nullable:true})
    lastName?: StringNullableFilter;

    @Field(() => StringNullableFilter, {nullable:true})
    patronymic?: StringNullableFilter;

    @Field(() => DateTimeNullableFilter, {nullable:true})
    dateBirth?: DateTimeNullableFilter;
}
