import { Field } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { PreUserWhereInput } from './pre-user-where.input';
import { Type } from 'class-transformer';
import { PreUserOrderByWithRelationInput } from './pre-user-order-by-with-relation.input';
import { PreUserWhereUniqueInput } from './pre-user-where-unique.input';
import { Int } from '@nestjs/graphql';
import { PreUserScalarFieldEnum } from './pre-user-scalar-field.enum';

@ArgsType()
export class FindFirstPreUserOrThrowArgs {

    @Field(() => PreUserWhereInput, {nullable:true})
    @Type(() => PreUserWhereInput)
    where?: PreUserWhereInput;

    @Field(() => [PreUserOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<PreUserOrderByWithRelationInput>;

    @Field(() => PreUserWhereUniqueInput, {nullable:true})
    cursor?: PreUserWhereUniqueInput;

    @Field(() => Int, {nullable:true})
    take?: number;

    @Field(() => Int, {nullable:true})
    skip?: number;

    @Field(() => [PreUserScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof PreUserScalarFieldEnum>;
}
