import { Field } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { PreUserWhereInput } from './pre-user-where.input';
import { Type } from 'class-transformer';

@ArgsType()
export class DeleteManyPreUserArgs {

    @Field(() => PreUserWhereInput, {nullable:true})
    @Type(() => PreUserWhereInput)
    where?: PreUserWhereInput;
}
