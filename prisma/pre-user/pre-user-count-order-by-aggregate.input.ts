import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SortOrder } from '../prisma/sort-order.enum';

@InputType()
export class PreUserCountOrderByAggregateInput {

    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    uniqueNumber?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    photoUrl?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    firstName?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    lastName?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    patronymic?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    dateBirth?: keyof typeof SortOrder;
}
