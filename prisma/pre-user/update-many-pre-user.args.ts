import { Field } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { PreUserUpdateManyMutationInput } from './pre-user-update-many-mutation.input';
import { Type } from 'class-transformer';
import { PreUserWhereInput } from './pre-user-where.input';

@ArgsType()
export class UpdateManyPreUserArgs {

    @Field(() => PreUserUpdateManyMutationInput, {nullable:false})
    @Type(() => PreUserUpdateManyMutationInput)
    data!: PreUserUpdateManyMutationInput;

    @Field(() => PreUserWhereInput, {nullable:true})
    @Type(() => PreUserWhereInput)
    where?: PreUserWhereInput;
}
