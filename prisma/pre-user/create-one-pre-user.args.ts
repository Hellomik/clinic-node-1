import { Field } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { PreUserCreateInput } from './pre-user-create.input';
import { Type } from 'class-transformer';

@ArgsType()
export class CreateOnePreUserArgs {

    @Field(() => PreUserCreateInput, {nullable:false})
    @Type(() => PreUserCreateInput)
    data!: PreUserCreateInput;
}
