import { Field } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { PreUserWhereUniqueInput } from './pre-user-where-unique.input';
import { Type } from 'class-transformer';
import { PreUserCreateInput } from './pre-user-create.input';
import { PreUserUpdateInput } from './pre-user-update.input';

@ArgsType()
export class UpsertOnePreUserArgs {

    @Field(() => PreUserWhereUniqueInput, {nullable:false})
    @Type(() => PreUserWhereUniqueInput)
    where!: PreUserWhereUniqueInput;

    @Field(() => PreUserCreateInput, {nullable:false})
    @Type(() => PreUserCreateInput)
    create!: PreUserCreateInput;

    @Field(() => PreUserUpdateInput, {nullable:false})
    @Type(() => PreUserUpdateInput)
    update!: PreUserUpdateInput;
}
