import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';

@InputType()
export class PreUserCreateManyInput {

    @Field(() => String, {nullable:true})
    id?: string;

    @Field(() => String, {nullable:false})
    uniqueNumber!: string;

    @Field(() => String, {nullable:false})
    photoUrl!: string;

    @Field(() => String, {nullable:true})
    firstName?: string;

    @Field(() => String, {nullable:true})
    lastName?: string;

    @Field(() => String, {nullable:true})
    patronymic?: string;

    @Field(() => Date, {nullable:true})
    dateBirth?: Date | string;
}
