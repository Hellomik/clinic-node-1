import { Field } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { SortOrder } from '../prisma/sort-order.enum';
import { PreUserCountOrderByAggregateInput } from './pre-user-count-order-by-aggregate.input';
import { PreUserMaxOrderByAggregateInput } from './pre-user-max-order-by-aggregate.input';
import { PreUserMinOrderByAggregateInput } from './pre-user-min-order-by-aggregate.input';

@InputType()
export class PreUserOrderByWithAggregationInput {

    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    uniqueNumber?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    photoUrl?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    firstName?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    lastName?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    patronymic?: keyof typeof SortOrder;

    @Field(() => SortOrder, {nullable:true})
    dateBirth?: keyof typeof SortOrder;

    @Field(() => PreUserCountOrderByAggregateInput, {nullable:true})
    _count?: PreUserCountOrderByAggregateInput;

    @Field(() => PreUserMaxOrderByAggregateInput, {nullable:true})
    _max?: PreUserMaxOrderByAggregateInput;

    @Field(() => PreUserMinOrderByAggregateInput, {nullable:true})
    _min?: PreUserMinOrderByAggregateInput;
}
