import { Field } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { PreUserWhereInput } from './pre-user-where.input';
import { Type } from 'class-transformer';
import { PreUserOrderByWithRelationInput } from './pre-user-order-by-with-relation.input';
import { PreUserWhereUniqueInput } from './pre-user-where-unique.input';
import { Int } from '@nestjs/graphql';
import { PreUserCountAggregateInput } from './pre-user-count-aggregate.input';
import { PreUserMinAggregateInput } from './pre-user-min-aggregate.input';
import { PreUserMaxAggregateInput } from './pre-user-max-aggregate.input';

@ArgsType()
export class PreUserAggregateArgs {

    @Field(() => PreUserWhereInput, {nullable:true})
    @Type(() => PreUserWhereInput)
    where?: PreUserWhereInput;

    @Field(() => [PreUserOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<PreUserOrderByWithRelationInput>;

    @Field(() => PreUserWhereUniqueInput, {nullable:true})
    cursor?: PreUserWhereUniqueInput;

    @Field(() => Int, {nullable:true})
    take?: number;

    @Field(() => Int, {nullable:true})
    skip?: number;

    @Field(() => PreUserCountAggregateInput, {nullable:true})
    _count?: PreUserCountAggregateInput;

    @Field(() => PreUserMinAggregateInput, {nullable:true})
    _min?: PreUserMinAggregateInput;

    @Field(() => PreUserMaxAggregateInput, {nullable:true})
    _max?: PreUserMaxAggregateInput;
}
