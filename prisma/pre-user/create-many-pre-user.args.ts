import { Field } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { PreUserCreateManyInput } from './pre-user-create-many.input';
import { Type } from 'class-transformer';

@ArgsType()
export class CreateManyPreUserArgs {

    @Field(() => [PreUserCreateManyInput], {nullable:false})
    @Type(() => PreUserCreateManyInput)
    data!: Array<PreUserCreateManyInput>;
}
