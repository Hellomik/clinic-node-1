import { Field } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { PreUserWhereUniqueInput } from './pre-user-where-unique.input';
import { Type } from 'class-transformer';

@ArgsType()
export class DeleteOnePreUserArgs {

    @Field(() => PreUserWhereUniqueInput, {nullable:false})
    @Type(() => PreUserWhereUniqueInput)
    where!: PreUserWhereUniqueInput;
}
