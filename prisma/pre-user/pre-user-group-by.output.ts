import { Field } from '@nestjs/graphql';
import { ObjectType } from '@nestjs/graphql';
import { PreUserCountAggregate } from './pre-user-count-aggregate.output';
import { PreUserMinAggregate } from './pre-user-min-aggregate.output';
import { PreUserMaxAggregate } from './pre-user-max-aggregate.output';

@ObjectType()
export class PreUserGroupBy {

    @Field(() => String, {nullable:false})
    id!: string;

    @Field(() => String, {nullable:false})
    uniqueNumber!: string;

    @Field(() => String, {nullable:false})
    photoUrl!: string;

    @Field(() => String, {nullable:true})
    firstName?: string;

    @Field(() => String, {nullable:true})
    lastName?: string;

    @Field(() => String, {nullable:true})
    patronymic?: string;

    @Field(() => Date, {nullable:true})
    dateBirth?: Date | string;

    @Field(() => PreUserCountAggregate, {nullable:true})
    _count?: PreUserCountAggregate;

    @Field(() => PreUserMinAggregate, {nullable:true})
    _min?: PreUserMinAggregate;

    @Field(() => PreUserMaxAggregate, {nullable:true})
    _max?: PreUserMaxAggregate;
}
