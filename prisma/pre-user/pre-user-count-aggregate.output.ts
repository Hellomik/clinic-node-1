import { Field } from '@nestjs/graphql';
import { ObjectType } from '@nestjs/graphql';
import { Int } from '@nestjs/graphql';

@ObjectType()
export class PreUserCountAggregate {

    @Field(() => Int, {nullable:false})
    id!: number;

    @Field(() => Int, {nullable:false})
    uniqueNumber!: number;

    @Field(() => Int, {nullable:false})
    photoUrl!: number;

    @Field(() => Int, {nullable:false})
    firstName!: number;

    @Field(() => Int, {nullable:false})
    lastName!: number;

    @Field(() => Int, {nullable:false})
    patronymic!: number;

    @Field(() => Int, {nullable:false})
    dateBirth!: number;

    @Field(() => Int, {nullable:false})
    _all!: number;
}
