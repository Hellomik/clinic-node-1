import { CacheModule, Module } from '@nestjs/common';
import { UserModule } from './modules/user/user.module';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { GraphQLModule } from '@nestjs/graphql';
import { ApolloDriver } from '@nestjs/apollo';
import { HttpModule } from '@nestjs/axios';
import { JwtModule } from '@nestjs/jwt';
import * as fs from 'fs';

import { join } from 'path';

const pathToPrivateKey = join(process.cwd(), 'private.pem');
const pathToPublicKey = join(process.cwd(), 'public.pem');

export const PrivateKey = fs.readFileSync(pathToPrivateKey, {
  encoding: 'utf-8',
});
export const PublicKey = fs.readFileSync(pathToPublicKey, {
  encoding: 'utf-8',
});
@Module({
  imports: [
    // PrismaModule,
    { ...HttpModule.register({}), global: true },
    ConfigModule.forRoot({ isGlobal: true }),
    CacheModule.register({
      isGlobal: true,
    }),
    UserModule,
    MongooseModule.forRoot(process.env.DATABASE_URL, {}),
    GraphQLModule.forRoot({
      debug: true,
      playground: true,
      driver: ApolloDriver,
      csrfPrevention: false,
      include: [],
      autoSchemaFile: true,
      sortSchema: true,
      uploads: true,
      context: ({ req, connection }: any) =>
        connection ? { req: connection.context } : { req },
      installSubscriptionHandlers: true,
    }),
    JwtModule.register({
      global: true,
    }),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
