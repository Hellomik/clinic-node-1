import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class UserBioData {
  @Field()
  firstName: string;
  @Field()
  lastName: string;
  @Field()
  patronymic: string;
  @Field(()=>Date)
  dateBirth: Date;
}
