import { Field, ObjectType } from '@nestjs/graphql';
import { User } from 'prisma/user/user.model';

@ObjectType()
export class AuthToken {
  @Field(() => String)
  accessToken: string;

  @Field(() => String)
  refreshToken: string;
}

@ObjectType()
export class UserAuthModel {
  @Field(() => User)
  user: User;

  @Field(() => AuthToken)
  token: AuthToken;
}
