import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { PreUserCreateInput } from 'prisma/pre-user/pre-user-create.input';
import { PreUser } from 'prisma/pre-user/pre-user.model';
import GraphQLUpload from 'src/graphql_upload/GraphQLUpload';
import { FileUpload } from 'src/graphql_upload/Upload';
import { PrismaService } from 'src/services/prisma.service';
import { RegisterService } from '../services/register.service';
import { HttpService } from '@nestjs/axios';
import { CACHE_MANAGER, Inject } from '@nestjs/common';
import { Cache } from 'cache-manager';
import { UserBioData } from '../model/user_bio_data.model';
import { PreUserWhereInput } from 'prisma/pre-user/pre-user-where.input';
import { User } from 'prisma/user/user.model';
import { UserCreateInput } from 'prisma/user/user-create.input';
import * as bcrypt from 'bcrypt';
import { saltOrRounds } from 'src/utils/balt';
import { AuthToken, UserAuthModel } from '../model/user_auth.model';
import { JwtService } from '@nestjs/jwt';
import { JWT_ACCESS_SECRET, JWT_REFRESH_SECRET } from 'src/utils/refresh_utils';

function makeid(length: number): string {
  let result = '';
  const characters = '0123456789';
  const charactersLength = characters.length;
  let counter = 0;
  while (counter < length) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
    counter += 1;
  }
  return result;
}

@Resolver()
export class RegisterResolver {
  constructor(
    public registerService: RegisterService,
    public prismaService: PrismaService,
    private readonly httpService: HttpService,
    @Inject(CACHE_MANAGER) private cacheManager: Cache,
    public jwtService: JwtService,
  ) {}

  @Query(() => Boolean)
  async isRegistered(
    @Args('uniqueNumber') uniqueNumber: string,
  ): Promise<boolean> {
    const user = await this.prismaService.user.findFirst({
      where: {
        uniqueNumber,
      },
    });
    const preUser = await this.prismaService.preUser.findFirst({
      where: {
        uniqueNumber,
      },
    });
    if (!preUser) throw 'No in Base';
    return !!user;
  }

  @Mutation(() => PreUser)
  async addPreUser(@Args('data') data: PreUserCreateInput) {
    const preUser = await this.prismaService.preUser.create({
      data: { ...data },
    });
    console.log(preUser);
    return preUser;
  }

  @Mutation(() => UserBioData)
  async checkBio(
    @Args('uniqueNumber') uniqueNumber: string,
    @Args('pipelines', {
      type: () => [GraphQLUpload],
    })
    pipelines: Promise<FileUpload>[],
  ): Promise<UserBioData> {
    PreUserWhereInput;
    const preUser = await this.prismaService.preUser.findFirst({
      where: {
        uniqueNumber,
      },
    });
    if (!preUser) throw 'User not found';
    const realPhotoResp = await this.httpService.axiosRef.get(
      preUser.photoUrl,
      {
        responseType: 'stream',
      },
    );

    const resultMathc = await this.registerService.checkINN(
      realPhotoResp.data,
      pipelines,
    );
    const mathcArray: number[] = resultMathc['match'];

    if (mathcArray.includes(0)) throw "Face doesn't match";
    return {
      firstName: preUser.firstName ?? '',
      lastName: preUser.lastName ?? '',
      patronymic: preUser.patronymic ?? '',
      dateBirth: preUser.dateBirth ?? new Date(),
    };
  }

  @Mutation(() => Boolean)
  async sendWbVerify(@Args('phoneNumber') phoneNumber: string) {
    const _id = makeid(5);
    await this.cacheManager.set(phoneNumber, _id, 60 * 10 * 1000);

    this.httpService.axiosRef
      .post('http://localhost:3100', {
        phone: phoneNumber,
        code: _id,
      })
      .catch((e) => console.log(e));

    return true;
  }

  @Mutation(() => Boolean)
  async checkWbVerify(
    @Args('phoneNumber') phoneNumber: string,
    @Args('code') code: string,
  ) {
    const realCode = await this.cacheManager.get(phoneNumber);
    if (realCode != code) return false;
    return true;
  }

  @Mutation(() => UserAuthModel)
  async fullForm(
    @Args('data', {
      type: () => UserCreateInput,
    })
    userCreate: UserCreateInput,

    @Args('password', {
      type: () => String,
    })
    password: string,
  ): Promise<UserAuthModel> {
    const createdUser = await this.prismaService.user.create({
      data: {
        ...userCreate,
        hash: await bcrypt.hash(password, saltOrRounds),
      },
    });
    createdUser.hash = '';
    createdUser.uniqueNumber;
    return {
      user: createdUser,
      token: {
        accessToken: this.jwtService.sign(
          {
            id: createdUser.id,
            uniqueNumber: createdUser.uniqueNumber,
          },
          {
            secret: JWT_ACCESS_SECRET,
            expiresIn: '30d',
          },
        ),
        refreshToken: this.jwtService.sign(
          {
            id: createdUser.id,
            uniqueNumber: createdUser.uniqueNumber,
          },
          {
            secret: JWT_REFRESH_SECRET,
            expiresIn: '100d',
          },
        ),
      },
    };
  }
}
