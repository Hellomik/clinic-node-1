import { UseGuards } from '@nestjs/common';
import { Resolver, Query } from '@nestjs/graphql';
import { User } from 'prisma/user/user.model';
import { CurrentTokenPayload, PreAuthGuard } from '../services/auth.service';
import { PrismaService } from 'src/services/prisma.service';

@Resolver()
export class UserResolver {
  constructor(public prismaService: PrismaService) {}

  @UseGuards(PreAuthGuard)
  @Query(() => User)
  async getUserData(
    @CurrentTokenPayload()
    { id }: Token,
  ): Promise<User> {
    const user = await this.prismaService.user.findFirst({
      where: {
        id,
      },
    });

    if (!user) throw 'User not found';
    return user;
  }
}
