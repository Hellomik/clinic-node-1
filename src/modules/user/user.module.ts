import { Module } from '@nestjs/common';
import { RegisterService } from './services/register.service';
import { RegisterResolver } from './resolvers/register.resolver';
import { PrismaService } from 'src/services/prisma.service';
import { LoginResolver } from './services/login.service';
import { UserResolver } from './resolvers/user.resolver';

@Module({
  imports: [],
  providers: [
    PrismaService,
    RegisterService,
    RegisterResolver,
    LoginResolver,
    UserResolver,
  ],
  exports: [],
})
export class UserModule {}
