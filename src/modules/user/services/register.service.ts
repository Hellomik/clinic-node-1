import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import GraphQLUpload from 'src/graphql_upload/GraphQLUpload';
import { FileUpload } from 'src/graphql_upload/Upload';
import * as FormData from 'form-data';

import { createWriteStream } from 'fs';
import { join } from 'path';

const create = async (image: FileUpload, filename: string) => {
  const { createReadStream } = image;
  return new Promise(async (resolve) => {
    createReadStream()
      .pipe(createWriteStream(join(process.cwd(), filename)))
      .on('finish', () => resolve(true))
      .on('error', () => {
        throw 'WTF';
      });
  });
};

@Resolver()
export class RegisterService {
  constructor(private readonly httpService: HttpService) {}

  async checkINN(realPhoto: any, chekPhoto: Promise<FileUpload>[]) {
    const uploads = await Promise.all(chekPhoto);
    const formData = new FormData();

    formData.append('file', realPhoto);

    uploads.forEach((e, i) => {
      formData.append('files', e.createReadStream(), {
        filename: new Date().toISOString() + i.toString() + '.jpeg',
      });
    });
    try {
      const postRequest = await this.httpService.axiosRef.post(
        'http://0.0.0.0:8080',
        formData,
        { headers: formData.getHeaders() },
      );

      console.log(postRequest.data);
      return postRequest.data;
    } catch (e) {
      console.log(e);
    }
  }
}
