import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { PrismaService } from 'src/services/prisma.service';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { JWT_ACCESS_SECRET, JWT_REFRESH_SECRET } from 'src/utils/refresh_utils';
import { UserAuthModel } from '../model/user_auth.model';

@Resolver()
export class LoginResolver {
  constructor(public prisma: PrismaService, public jwtService: JwtService) {}

  @Mutation(() => UserAuthModel)
  async login(
    @Args('uniqueNumber') uniqueNumber: string,
    @Args('password') password: string,
  ): Promise<UserAuthModel> {
    const user = await this.prisma.user.findFirst({
      where: {
        uniqueNumber,
      },
    });
    if (!user) throw 'User not found';
    const hash = user.hash;
    user.hash = '';
    if (!(await bcrypt.compare(password, hash))) throw 'Wrong password';

    return {
      user: user,
      token: {
        accessToken: this.jwtService.sign(
          {
            id: user.id,
            uniqueNumber: user.uniqueNumber,
          },
          {
            secret: JWT_ACCESS_SECRET,
            expiresIn: '30d',
          },
        ),
        refreshToken: this.jwtService.sign(
          {
            id: user.id,
            uniqueNumber: user.uniqueNumber,
          },
          {
            secret: JWT_REFRESH_SECRET,
            expiresIn: '100d',
          },
        ),
      },
    };
  }
}
