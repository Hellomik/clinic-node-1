import {
  CanActivate,
  createParamDecorator,
  ExecutionContext,
  Injectable,
} from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { JwtService } from '@nestjs/jwt';
import { User } from '@prisma/client';
import { JWT_ACCESS_SECRET } from 'src/utils/refresh_utils';

@Injectable()
export class EasyPreAuthGuard implements CanActivate {
  constructor(private tokenService: JwtService) {}

  async canActivate(context: ExecutionContext) {
    const request = GqlExecutionContext.create(context).getContext().req;
    const { authorization } = request.headers;
    if (!authorization) return false;
    try {
      const payload:
        | Partial<User>
        | {
            [key: string]: any;
          } = this.tokenService.verify(authorization, {
        secret: JWT_ACCESS_SECRET,
      });
      request.payload = payload;
      return true;
    } catch (e) {
      throw `${e}`;
    }
  }
}

@Injectable()
export class PreAuthGuard implements CanActivate {
  constructor(private tokenService: JwtService) {}

  async canActivate(context: ExecutionContext) {
    const request = GqlExecutionContext.create(context).getContext().req;
    const { authorization } = request.headers;
    if (!authorization) return false;
    try {
      const payload:
        | Partial<User>
        | {
            [key: string]: any;
          } = this.tokenService.verify(authorization, {
        secret: JWT_ACCESS_SECRET,
      });
      request.payload = payload;
      if (payload.id == null) throw 'BAD HACKER';

      return true;
    } catch (e) {
      throw `${e}`;
    }
  }
}

export const CurrentTokenPayload = createParamDecorator(
  async (data: unknown, context: ExecutionContext) => {
    const request = GqlExecutionContext.create(context).getContext().req;
    return request.payload;
  },
);
